import os
import connexion
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager


db = SQLAlchemy()
bcrypt = Bcrypt()
jwt = JWTManager()

user = os.environ["POSTGRES_USER"]
password = os.environ["POSTGRES_PASSWORD"]
host = os.environ["POSTGRES_HOST"]
database = os.environ["POSTGRES_DB"]
port = os.environ["POSTGRES_PORT"]


def get_app():
    connex_app = connexion.App(__name__, specification_dir='../')
    connex_app.add_api('security.yml')
    app = connex_app.app
    app.secret_key = 'xxxxvcmxxxx'
    app.config['JWT_SECRET_KEY'] = 'xxxxvcmxxxx'
    app.config['JWT_ALGORITHM'] = 'RS256'
    app.config['JWT_PRIVATE_KEY'] = open('tools/rsa_keys/rs256.pem').read()
    app.config['JWT_PUBLIC_KEY'] = open('tools/rsa_keys/rs256.pub').read()
    app.config['JWT_DECODE_ALGORITHMS'] = 'RS256'
    app.config['SQLALCHEMY_DATABASE_URI'] = f"postgresql://{user}:{password}@{host}:{port}/{database}"
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)
    bcrypt.init_app(app)
    jwt.init_app(app)

    return app
