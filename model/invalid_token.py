from config.app_config import db


class InvalidToken(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    token = db.Column(db.String, nullable=False, unique=True)

    def __init__(self, token):
        self.token = token

    def to_json(self):
        return {
            "id": self.id,
            "token": self.token
        }
