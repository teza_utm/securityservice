from config.app_config import db
from sqlalchemy import JSON
from model.regular_token import RegularToken


class RegularAccount(db.Model):
    __tablename__ = "regular_account"
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String, nullable=False, unique=True)
    password = db.Column(db.String, nullable=False)
    token = db.relationship('RegularToken', backref='regular_account', lazy=True, cascade="all, delete, delete-orphan")
    profile = db.Column(JSON, nullable=True)
    role = db.Column(db.String, nullable=False, server_default="Watcher")
    enabled = db.Column(db.Boolean, server_default="false")

    def __init__(self, password, email):
        self.password = password
        self.email = email
        self.profile = {}

    def to_json(self):
        return {"id": self.id,
                "email": self.email,
                "profile": self.profile,
                "role": self.role,
                "enabled": self.enabled}
