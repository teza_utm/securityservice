from config.app_config import db


class RegularToken(db.Model):
    __tablename__ = "regular_token"
    id = db.Column(db.Integer, primary_key=True)
    account_id = db.Column(db.Integer, db.ForeignKey('regular_account.id'), nullable=False)
    token = db.Column(db.String, nullable=False, unique=True)

    def __init__(self, token, account):
        self.token = token
        self.account_id = account.id

    def to_json(self):
        return {
            "id": self.id,
            "token": self.token
        }
