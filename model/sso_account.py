from config.app_config import db
from sqlalchemy import JSON
from model.sso_token import SSOToken


class SSOAccount(db.Model):
    __tablename__ = "sso_account"
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String, nullable=False, unique=True)
    token = db.relationship('SSOToken', backref='sso_account', lazy=True, cascade="all, delete, delete-orphan")
    profile = db.Column(JSON, nullable=True)
    role = db.Column(db.String, nullable=False, server_default="Watcher")
    enabled = db.Column(db.Boolean, nullable=False, server_default="false")

    def __init__(self, email):
        self.email = email
        self.profile = {}

    def to_json(self):
        return {"id": self.id,
                "email": self.email,
                "profile": self.profile,
                "role": self.role,
                "enabled": self.enabled}
