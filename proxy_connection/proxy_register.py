import time
import threading
from model.discovery import Discovery
from model.endpoint import Endpoint
import requests
import os
import socket
import json


proxy_host = os.environ["PROXY_HOST"]


def register_thread():
    while True:
        endpoint_list = get_endpoint_list()
        discovery = Discovery(address=get_address(), endpoints=endpoint_list)
        requests.post(f"http://{proxy_host}:4000/register", data=json.dumps(discovery, default=lambda o: o.__dict__))
        time.sleep(10)


def register_service():
    processing_thread = threading.Thread(target=register_thread)
    processing_thread.start()


def get_endpoint_list():
    return [
        Endpoint(path="/user", method="POST", cache_timeout=0),
        Endpoint(path="/user", method="GET", cache_timeout=0),
        Endpoint(path="/login", method="POST", cache_timeout=0),
        Endpoint(path="/google/login", method="GET", cache_timeout=0),
        Endpoint(path="/oauth2callback", method="GET", cache_timeout=0),
        Endpoint(path="/logout", method="GET", cache_timeout=0),
        Endpoint(path="/user", method="DELETE", cache_timeout=0),
        Endpoint(path="/user", method="PATCH", cache_timeout=0),
        Endpoint(path="/user/token", method="GET", cache_timeout=0),
        Endpoint(path="/users", method="GET", cache_timeout=0),
        Endpoint(path="/user/profile", method="PUT", cache_timeout=0),
        Endpoint(path="/user/admin", method="GET", cache_timeout=0),
        Endpoint(path="/user/promote", method="PUT", cache_timeout=0),
        Endpoint(path="/user/demote", method="PUT", cache_timeout=0),
        Endpoint(path="/user/activate", method="PUT", cache_timeout=0),
        Endpoint(path="/user/deactivate", method="PUT", cache_timeout=0),
        Endpoint(path="/security/api/", method="GET", cache_timeout=0)
        ]


def get_local_ip():
    hostname = socket.gethostname()
    return socket.gethostbyname(hostname)


def get_address():
    return f'{get_local_ip()}:{int(os.environ["PORT"])}'
