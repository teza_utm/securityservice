openapi: "3.0.0"
info:
  description: Service responsible for user account manipulations
  version: "1.0.0"
  title: Security Service
servers:
  - url: http://localhost:3000/security/api

paths:
  /user:
    delete:
      security:
        - bearerAuth: []
      operationId: service.account_service.delete
      tags:
        - Admin
      summary: Delete user based on email
      description: ""
      parameters:
        - in: query
          name: email
          required: true
          description: user email
          schema:
            type: string
            example: test@gmail.com
      responses:
        200:
          description: Successful user account delition
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SuccessMessage'
        404:
          description: Resource not found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InexistentEmailMessage'
        x-400:
          description: Query error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/WrongValueMessage'
        401:
          description: Query error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MissingAuthorizationHeaderMessage'
        x-401:
          description: Unauthorized access
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RoleRequiredMessage'

    post:
      operationId: service.account_service.register
      tags:
        - No role
      summary: Register user
      description: ""
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserAuth'
      responses:
        200:
          description: Successful user registration
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SuccessMessage'
        400:
          description: Query error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ExistentEmailMessage'
        x-400:
          description: Invalid body
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/WrongBodyMessage'

    patch:
      security:
        - bearerAuth: []
      operationId: service.account_service.update_user_password
      tags:
        - Watcher
      summary: Update user password
      description: ""
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/PasswordChange'
      responses:
        200:
          description: Successful password update
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SuccessMessage'
        403:
          description: Wrong credentials
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/WrongOldPasswordMessage'
        x-400:
          description: Invalid body
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/WrongBodyMessage'
        401:
          description: Query error
          content:
            application/json:
              schema:
               $ref: '#/components/schemas/MissingAuthorizationHeaderMessage'
    get:
      security:
        - bearerAuth: []
      operationId: service.profile_service.get_user_data
      tags:
        - Watcher
      summary: Get user data
      description: ""
      responses:
        200:
          description: Successful user data retrieve
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/User'
                  - type: object
                    properties:
                      enabled:
                          type: "boolean"
                          example: True
        401:
          description: Query error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MissingAuthorizationHeaderMessage'

  /login:
    post:
      operationId: service.account_service.login
      tags:
        - No role
      summary: Login regular user
      description: ""
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserAuth'
      responses:
        200:
          description: Successful user login
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Token'
        401:
          description: Wrong Credentials
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/WrongCredentialsMessage'
        400:
          description: Invalid body
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/WrongBodyMessage'
        403:
          description: User not enabled
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/NotEnabledMessage'

  /google/login:
    get:
      operationId: service.sso_service.google_login
      tags:
        - No role
      summary: Login user using Google Sign-in
      description: ""
      responses:
        200:
          description: Successful user login
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Token'
        401:
          description: Wrong Credentials
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/WrongCredentialsMessage'
        400:
          description: Invalid body
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/WrongBodyMessage'

  /logout:
    get:
      security:
        - bearerAuth: []
      operationId: service.account_service.logout
      tags:
        - Watcher
      summary: Logout user
      description: ""
      responses:
        200:
          description: Successful user logout
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SuccessMessage'
        401:
          description: Query error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MissingAuthorizationHeaderMessage'

  /user/token:
    get:
      security:
        - bearerAuth: []
      operationId: service.account_service.get_token_validity
      tags:
        - Watcher
      summary: Check token validity
      description: ""
      responses:
        200:
          description: Successful token validation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ValidToken'
        401:
          description: Query error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MissingAuthorizationHeaderMessage'
        x-401:
          description: Invalid token
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InvalidToken'

  /user/admin:
    get:
      security:
        - bearerAuth: []
      operationId: service.role_service.has_user_admin_role
      tags:
        - Watcher
      summary: Check admin status of the user
      description: ""
      responses:
        200:
          description: Successful admin status validation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AdminValidation'
        401:
          description: Query error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MissingAuthorizationHeaderMessage'

  /user/activate:
    patch:
      security:
        - bearerAuth: []
      operationId: service.role_service.activate
      tags:
        - Admin
      summary: Enable user account
      description: ""
      parameters:
        - in: query
          name: email
          required: true
          description: user email
          schema:
            type: string
            example: test@gmail.com
      responses:
        200:
          description: Successful user account activation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SuccessMessage'
        404:
          description: Resource not found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InexistentEmailMessage'
        401:
          description: Query error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MissingAuthorizationHeaderMessage'
        x-401:
          description: Unauthorized access
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RoleRequiredMessage'

  /user/deactivate:
    put:
      security:
        - bearerAuth: []
      operationId: service.role_service.deactivate
      tags:
        - Admin
      summary: Disable user account
      description: ""
      parameters:
        - in: query
          name: email
          required: true
          description: user email
          schema:
            type: string
            example: test@gmail.com
      responses:
        200:
          description: Successful user account deactivation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SuccessMessage'
        404:
          description: Resource not found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InexistentEmailMessage'
        401:
          description: Query error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MissingAuthorizationHeaderMessage'
        x-401:
          description: Query error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RoleRequiredMessage'

  /user/promote:
    put:
      security:
        - bearerAuth: []
      operationId: service.role_service.promote
      tags:
        - Admin
      summary: Promote user to admin
      description: ""
      parameters:
        - in: query
          name: email
          required: true
          description: user email
          schema:
            type: string
            example: test@gmail.com
      responses:
        200:
          description: Successful promotion to admin
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SuccessMessage'
        404:
          description: Resource not found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InexistentEmailMessage'
        401:
          description: Query error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MissingAuthorizationHeaderMessage'
        x-401:
          description: Query error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RoleRequiredMessage'

  /user/demote:
    put:
      security:
        - bearerAuth: []
      operationId: service.role_service.demote
      tags:
        - Admin
      summary: Demote user to watcher
      description: ""
      parameters:
        - in: query
          name: email
          required: true
          description: user email
          schema:
            type: string
            example: test@gmail.com
      responses:
        200:
          description: Successful demotion to watcher
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SuccessMessage'
        404:
          description: Resource not found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InexistentEmailMessage'
        401:
          description: Query error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MissingAuthorizationHeaderMessage'
        x-401:
          description: Query error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RoleRequiredMessage'

  /user/profile:
    patch:
      security:
        - bearerAuth: []
      operationId: service.profile_service.add_profile_settings
      tags:
        - Watcher
      summary: Add user profile custome settings
      description: ""
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ProfileSettings'
      responses:
        200:
          description: Successful addition of user account custome settings
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SuccessMessage'
        401:
          description: Query error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MissingAuthorizationHeaderMessage'

  /users:
    get:
      security:
        - bearerAuth: []
      operationId: service.profile_service.get_users
      tags:
        - Admin
      summary: Get all users data
      description: ""
      responses:
        200:
          description: Successful retrieve of user data
          content:
            application/json:
              schema:
                type: array
                items:
                  allOf:
                    - $ref: '#/components/schemas/User'
                    - type: object
                      properties:
                        enabled:
                            type: "boolean"
                            example: True
        401:
          description: Query error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MissingAuthorizationHeaderMessage'
        x-401:
          description: Query error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RoleRequiredMessage'

components:
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT
      x-bearerInfoFunc: tools.token.decode_user_token

  schemas:
    User:
      type: object
      properties:
        id:
          type: "integer"
          example: 1
        email:
          type: "string"
          example: "test@gmail.com"
        role:
          type: "string"
          example: "Watcher"
        profile:
          type: object
          properties:
            field1:
              type: "string"
              example: "data1"
            field2:
              type: "string"
              example: "data2"

    UserAuth:
      type: object
      properties:
        email:
          type: "string"
          example: "test@gmail.com"
        password:
          type: "string"
          example: "test"

    Token:
      type: object
      properties:
        token:
          type: "string"
          example: "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9"

    PasswordChange:
      type: object
      properties:
        old_password:
          type: "string"
          example: "test"
        new_password:
          type: "string"
          example: "test123"

    ValidToken:
      type: object
      properties:
        message:
          type: "string"
          example: "Valid token"

    InvalidToken:
      type: object
      properties:
        message:
          type: "string"
          example: "Invalid token"

    ProfileSettings:
      type: object
      properties:
        field1:
          type: "string"
          example: "settings"
        field2:
          type: "array"
          items:
            type: "integer"
          example: [1, 2, 3]
        field3:
          type: object

    AdminValidation:
      type: object
      properties:
        is_admin:
          type: "boolean"
          example: True

    SuccessMessage:
      type: object
      properties:
        message:
          type: "string"
          example: "Success"

    WrongValueMessage:
      type: object
      properties:
        message:
          type: "string"
          example: "Wrong value. Check documentation!"

    RoleRequiredMessage:
      type: object
      properties:
        message:
          type: "string"
          example: "Admin role required!"

    InexistentEmailMessage:
      type: object
      properties:
        message:
          type: "string"
          example: "User with such email does not exist!"

    MissingAuthorizationHeaderMessage:
      type: object
      properties:
        message:
          type: "string"
          example: "Missing Authorization Header"

    WrongBodyMessage:
      type: object
      properties:
        message:
          type: "string"
          example: "Wrong body. Check documentation!"

    ExistentEmailMessage:
      type: object
      properties:
        message:
          type: "string"
          example: "Email already in use!"

    WrongCredentialsMessage:
      type: object
      properties:
        message:
          type: "string"
          example: "Wrong credentials! Email or password is incorrect!"

    NotEnabledMessage:
      type: object
      properties:
        message:
          type: "string"
          example: "User is not enabled!"

    WrongOldPasswordMessage:
      type: object
      properties:
        message:
          type: "string"
          example: "Old password is not correct!"
