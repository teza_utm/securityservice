import os
import json
from config.app_config import get_app
from flask import request, Response, redirect
from service.account_service import login, register, logout, delete, get_token_validity, update_user_password
from service.role_service import has_user_admin_role, promote, demote, activate, deactivate
from service.profile_service import get_user_data, get_users, add_profile_settings
from service.sso_service import google_login, google_redirect, init_app, google_login_second, google_redirect_second
from proxy_connection import proxy_register


app = get_app()
init_app(app)


@app.route('/user', methods=["POST"])
def register_user():
    request_payload = json.loads(request.data)
    return Response(json.dumps(register(request_payload)), mimetype="application/json")


@app.route('/user', methods=["GET"])
def get_user():
    return Response(json.dumps(get_user_data()), mimetype="application/json")


@app.route('/login', methods=["POST"])
def login_user():
    request_payload = json.loads(request.data)
    return Response(json.dumps(login(request_payload)), mimetype="application/json")


@app.route('/google/login', methods=["GET"])
def google_login_user():
    return google_login()


@app.route("/oauth2callback", methods=["GET"])
def google_authorize():
    return Response(json.dumps(google_redirect()), mimetype="application/json")

@app.route('/google/login/second', methods=["GET"])
def google_login_user_second():
    return google_login_second()


@app.route("/oauth2callback/second", methods=["GET"])
def google_authorize_second():
    token = google_redirect_second()
    url = f'{os.environ["SSO_REDIRECT"]}?token={token}'

    return redirect(url)


@app.route('/logout', methods=["GET"])
def logout_user():
    return Response(json.dumps(logout()), mimetype="application/json")


@app.route('/user', methods=["DELETE"])
def delete_user():
    return Response(json.dumps(delete()), mimetype="application/json")


@app.route('/user', methods=["PATCH"])
def update_password():
    request_payload = json.loads(request.data)
    return Response(json.dumps(update_user_password(request_payload)), mimetype="application/json")


@app.route('/user/token', methods=["GET"])
def get_token():
    return Response(json.dumps(get_token_validity()), mimetype="application/json")


@app.route('/users', methods=["GET"])
def get_all_users():
    return Response(json.dumps(get_users()), mimetype="application/json")


@app.route('/user/profile', methods=["PATCH"])
def add_user_profile_settings():
    request_payload = json.loads(request.data)
    return Response(json.dumps(add_profile_settings(request_payload)), mimetype="application/json")


@app.route('/user/admin', methods=["GET"])
def get_user_role():
    return Response(json.dumps(has_user_admin_role()), mimetype="application/json")


@app.route('/user/promote', methods=["PATCH"])
def promote_user():
    return Response(json.dumps(promote()), mimetype="application/json")


@app.route('/user/demote', methods=["PATCH"])
def demote_user():
    return Response(json.dumps(demote()), mimetype="application/json")


@app.route('/user/activate', methods=["PATCH"])
def activate_user():
    return Response(json.dumps(activate()), mimetype="application/json")


@app.route('/user/deactivate', methods=["PATCH"])
def deactivate_user():
    return Response(json.dumps(deactivate()), mimetype="application/json")


if __name__ == "__main__":
    proxy_register.register_service()
    app.run(host="0.0.0.0", port=int(os.environ["PORT"]), debug=True, threaded=True, use_reloader=False)
