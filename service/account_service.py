import json
from tools.validation import does_email_exist, is_password_valid
from model.regular_account import RegularAccount
from model.regular_token import RegularToken
from model.invalid_token import InvalidToken
from config.app_config import db, bcrypt, jwt
from flask import abort, make_response, request
from service.role_service import admin_required
from functools import wraps
from flask_jwt_extended import jwt_required, get_jwt_identity, get_jwt, verify_jwt_in_request
from tools.token import create_user_token, get_request_token, get_user_by_token, invalidate_token
from tools.user import get_user_by_email


@jwt.token_in_blocklist_loader
def check_if_token_is_revoked(jwt_header, jwt_payload):
    token = get_request_token(request)
    token_in_blocklist = InvalidToken.query.filter_by(token=token).first()

    return token_in_blocklist is not None


def enabled_user(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        request_payload = json.loads(request.data)
        try:
            email = request_payload["email"]
            user = get_user_by_email(email)

            if user:
                if user.enabled:
                    return f(*args, **kwargs)
                else:
                    abort(make_response('{"message": "User is not enabled!"}', 403))
            else:
                abort(make_response('{"message": "Wrong credentials! Email or password is incorrect!"}', 401))
        except KeyError:
            abort(make_response('{"message": "Wrong body. Check documentation!"}', 400))
    return wrap


@enabled_user
def login(body):
    try:
        email = body["email"]
        password = body["password"]

        user = RegularAccount.query.filter_by(email=email).first()
        if user and is_password_valid(user.password, password):
            user_token = create_user_token(email)
            regular_token = RegularToken(user_token, user)
            db.session.add(regular_token)
            db.session.commit()
            return {"token": user_token}
        else:
            abort(make_response('{"message": "Wrong credentials! Email or password is incorrect!"}', 401))
    except KeyError:
        abort(make_response('{"message": "Wrong body. Check documentation!"}', 400))


def register(body):
    try:
        email = body["email"]
        password = body["password"]

        if not does_email_exist(email, RegularAccount):
            hashed_password = bcrypt.generate_password_hash(password).decode('utf8')
            new_user = RegularAccount(email=email, password=hashed_password)
            db.session.add(new_user)
            db.session.commit()
            return {"message": "Success"}
        else:
            abort(make_response('{"message": "Email already in use!"}', 400))
    except KeyError:
        abort(make_response('{"message": "Wrong body. Check documentation!"}', 400))


@jwt_required()
def logout():
    token = get_request_token(request)
    invalid_token = InvalidToken(token)
    db.session.add(invalid_token)
    db.session.commit()

    return {"message": "Success"}


@jwt_required()
@admin_required
def delete():
    try:
        user_email = request.args.get('email')
        user = get_user_by_email(user_email)
        if not user:
            abort(make_response('{"message": "User with such email does not exist!"}', 404))

        db.session.delete(user)
        db.session.commit()
        return {"message": "Success"}
    except Exception:
        abort(make_response('{"message": "Wrong value. Check documentation!"}', 400))


@jwt_required()
def update_user_password(body):
    try:
        old_password = body["old_password"]
        new_password = body["new_password"]

        token = get_request_token(request)
        user = get_user_by_token(token, RegularAccount, RegularToken)

        if is_password_valid(user.password, old_password):
            hashed_password = bcrypt.generate_password_hash(new_password).decode('utf8')
            user.password = hashed_password
            db.session.commit()
            return {"message": "Success"}
        else:
            abort(make_response('{"message": "Old password is not correct!"}', 403))
    except KeyError:
        abort(make_response('{"message": "Wrong body. Check documentation!"}', 400))


@jwt_required()
def get_token_validity():
    try:
        verify_jwt_in_request()
        identity = get_jwt_identity()
        if identity:
            return {"message": f"Valid token"}
    except Exception:
        abort(make_response('{"message": "Invalid token!"}', 401))

