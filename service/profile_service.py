from flask_jwt_extended import jwt_required
from flask import request
from tools.token import get_request_token, get_user_by_token
from tools.user import get_all_account_user
from service.role_service import admin_required
from config.app_config import db
from model.regular_account import RegularAccount
from model.sso_account import SSOAccount


@jwt_required()
def get_user_data():
    token = get_request_token(request)
    user = get_user_by_token(token)

    return user.to_json()


@jwt_required()
@admin_required
def get_users():
    regular_users = get_all_account_user(RegularAccount)
    sso_users = get_all_account_user(SSOAccount)
    total_users = regular_users + sso_users

    return total_users


@jwt_required()
def add_profile_settings(body):
    token = get_request_token(request)
    user = get_user_by_token(token)
    user.profile = body
    db.session.commit()

    return {"message": "Success"}