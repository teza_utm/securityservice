from functools import wraps
from flask_jwt_extended import jwt_required
from flask import abort, make_response, request
from config.app_config import db
from tools.token import get_request_token, get_user_by_token, get_user_token, invalidate_token
from tools.user import get_user_by_email


def admin_required(func):
    @wraps(func)
    def wrap(*args, **kwargs):
        is_admin = has_user_admin_role()["is_admin"]
        if is_admin:
            return func(*args, **kwargs)
        else:
            abort(make_response('{"message": "Admin role required!"}', 401))
    return wrap


@jwt_required()
def has_user_admin_role():
    token = get_request_token(request)
    user = get_user_by_token(token)

    is_admin = False
    if user.role == "Admin":
        is_admin = True

    return {
        "is_admin": is_admin
    }


@jwt_required()
@admin_required
def promote():
    user_email = request.args.get('email')
    user = get_user_by_email(user_email)
    if not user:
        abort(make_response('{"message": "User with such email does not exist!"}', 404))

    user.role = "Admin"
    db.session.commit()

    return {"message": "Success"}


@jwt_required()
@admin_required
def demote():
    user_email = request.args.get('email')
    user = get_user_by_email(user_email)
    if not user:
        abort(make_response('{"message": "User with such email does not exist!"}', 404))

    user.role = "Watcher"
    db.session.commit()

    return {"message": "Success"}


@jwt_required()
@admin_required
def activate():
    user_email = request.args.get('email')
    user = get_user_by_email(user_email)
    if not user:
        abort(make_response('{"message": "User with such email does not exist!"}', 404))

    user.enabled = True
    db.session.commit()

    return {"message": "Success"}


@jwt_required()
@admin_required
def deactivate():
    user_email = request.args.get('email')
    user = get_user_by_email(user_email)
    if not user:
        abort(make_response('{"message": "User with such email does not exist!"}', 404))

    user.enabled = False
    token = get_user_token(user)
    invalid_token = invalidate_token(token)
    if invalid_token:
        db.session.add(invalid_token)
    db.session.commit()

    return {"message": "Success"}