from flask import url_for
from authlib.integrations.flask_client import OAuth
from config.app_config import db
from tools.token import create_user_token
from model.sso_token import SSOToken
from tools.validation import does_email_exist
from model.sso_account import SSOAccount


oauth = OAuth()


def init_app(app):
    oauth.init_app(app)


google = oauth.register(
    name='google',
    client_id='831187405514-o40olmuog32mt93ir623dkr488i20st9.apps.googleusercontent.com',
    client_secret='zg1MkTXiBBahiiftQecdj-eS',
    access_token_url='https://accounts.google.com/o/oauth2/token',
    access_token_params=None,
    authorize_url='https://accounts.google.com/o/oauth2/auth',
    authorize_params=None,
    api_base_url='https://www.googleapis.com/oauth2/v1/',
    userinfo_endpoint='https://openidconnect.googleapis.com/v1/userinfo',
    client_kwargs={'scope': 'openid email profile'},
)

google_second = oauth.register(
    name='google',
    client_id='831187405514-nceaetoamb93av7m621e8dtle8jtfcet.apps.googleusercontent.com',
    client_secret='ZP7BRbEiNlNXiNku8_l6OK4Z',
    access_token_url='https://accounts.google.com/o/oauth2/token',
    access_token_params=None,
    authorize_url='https://accounts.google.com/o/oauth2/auth',
    authorize_params=None,
    api_base_url='https://www.googleapis.com/oauth2/v1/',
    userinfo_endpoint='https://openidconnect.googleapis.com/v1/userinfo',
    client_kwargs={'scope': 'openid email profile'},
)


def google_login_second():
    google_second = oauth.create_client('google')
    redirect_uri = url_for('google_authorize', _external=True)
    return google_second.authorize_redirect(f"{redirect_uri}/second")


def google_redirect_second():
    google_second = oauth.create_client('google')
    token = google_second.authorize_access_token()
    resp = google_second.get('userinfo')
    user_info = resp.json()
    user = oauth.google.userinfo()
    email = user["email"]
    user_token = create_user_token(email)

    if not does_email_exist(email, SSOAccount):
        register(email)

    account = SSOAccount.query.filter_by(email=email).first()

    sso_token = SSOToken(user_token, account)
    db.session.add(sso_token)
    db.session.commit()
    return user_token


def google_login():
    google = oauth.create_client('google')
    redirect_uri = url_for('google_authorize', _external=True)
    return google.authorize_redirect(redirect_uri)


def google_redirect():
    google = oauth.create_client('google')
    token = google.authorize_access_token()
    resp = google.get('userinfo')
    user_info = resp.json()
    user = oauth.google.userinfo()
    email = user["email"]
    user_token = create_user_token(email)

    if not does_email_exist(email, SSOAccount):
        register(email)

    account = SSOAccount.query.filter_by(email=email).first()

    sso_token = SSOToken(user_token, account)
    db.session.add(sso_token)
    db.session.commit()

    return {"token": user_token}


def register(email):
    new_user = SSOAccount(email=email)
    db.session.add(new_user)
    db.session.commit()