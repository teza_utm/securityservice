import datetime
from flask_jwt_extended import create_access_token, decode_token
from model.sso_token import SSOToken
from model.regular_token import RegularToken
from model.sso_account import SSOAccount
from model.regular_account import RegularAccount
from model.invalid_token import InvalidToken


def create_user_token(email):
    expires = datetime.timedelta(days=1)
    access_token = create_access_token(identity=email,
                                       expires_delta=expires)
    return access_token


def get_request_token(request):
    request_headers = request.headers
    token = request_headers["Authorization"].split(" ")[-1]
    return token


def get_user_by_token(user_token, account_table=None, token_table=None):
    if account_table and token_table:
        token = token_table.query.filter_by(token=user_token).first()
        user = account_table.query.get(token.account_id)
    else:
        token = RegularToken.query.filter_by(token=user_token).first()
        if token:
            user = RegularAccount.query.get(token.account_id)
        else:
            token = SSOToken.query.filter_by(token=user_token).first()
            user = SSOAccount.query.get(token.account_id)
    return user


def decode_user_token(token):
    return decode_token(token)


def get_user_token(user):
    user_id = user.id
    token = RegularToken.query.filter_by(account_id=user_id)[-1]
    if not token:
        token = SSOToken.query.filter_by(account_id=user_id)[-1]

    return token.token


def invalidate_token(token):
    token = InvalidToken.query.filter_by(token=token)
    if not token:
        return InvalidToken(token)

