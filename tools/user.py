from tools.validation import does_email_exist
from model.regular_account import RegularAccount
from model.sso_account import SSOAccount


def get_user_by_email(email):
    user = False
    if does_email_exist(email, RegularAccount):
        user = RegularAccount.query.filter_by(email=email).first()
    elif does_email_exist(email, SSOAccount):
        user = SSOAccount.query.filter_by(email=email).first()
    return user


def get_all_account_user(account_table):
    total_users = []
    regular_users = account_table.query.all()
    for regular_user in regular_users:
        total_users.append(regular_user.to_json())
    return total_users
