from config.app_config import bcrypt


def does_email_exist(email, account_table):
    user_email = account_table.query.filter_by(email=email).first()
    email_exists = False

    if user_email:
        email_exists = True
    return email_exists


def is_password_valid(user_password, current_password):
    return bcrypt.check_password_hash(user_password, current_password)
